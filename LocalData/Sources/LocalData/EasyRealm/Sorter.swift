//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public struct Sorter {
    public enum Order {
        case ascending
        case descending
    }

    let keyPath: String
    let order: Order

    public init(keyPath: String, order: Order) {
        self.keyPath = keyPath
        self.order = order
    }
}
