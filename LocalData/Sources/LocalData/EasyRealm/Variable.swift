//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import Foundation
import Realm
import RealmSwift

extension EasyRealm where T: Object {

    public var isManaged: Bool {
        return (self.base.realm != nil)
    }

    public var unmanaged: T {
        return self.base.easyDetached()
    }
}

private extension Object {
    func easyDetached() -> Self {
        let detached = type(of: self).init()
        for property in objectSchema.properties {
            guard let value = value(forKey: property.name) else { continue }
            if let detachable = value as? Object {
                detached.setValue(detachable.easyDetached(), forKey: property.name)
            } else if let detachable = value as? EasyRealmList {
                detached.setValue(detachable.children().compactMap { $0.easyDetached() }, forKey: property.name)
            } else {
                detached.setValue(value, forKey: property.name)
            }
        }
        return detached
    }
}
