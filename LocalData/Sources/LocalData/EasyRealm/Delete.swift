//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import Foundation
import Realm
import RealmSwift

public enum EasyRealmDeleteMethod {
    case simple
    case cascade
}

public extension EasyRealm where T: Object {

    func delete(with method: EasyRealmDeleteMethod = .simple) throws {
        switch method {
        case .simple: self.isManaged ? try managedSimpleDelete() : try unmanagedSimpleDelete()
        case .cascade: self.isManaged ? try managedCascadeDelete() : try unmanagedCascadeDelete()
        }
    }
}

// Normal Way
private extension EasyRealm where T: Object {

    func managedSimpleDelete() throws {
        guard let rq = EasyRealmQueue() else { throw EasyRealmError.realmQueueCantBeCreate }
        let ref = ThreadSafeReference(to: self.base)
        try rq.queue.sync {
            guard let object = rq.realm.resolve(ref) else { throw EasyRealmError.objectCantBeResolved }
            try rq.realm.write {
                EasyRealm.simpleDelete(this: object, in: rq)
            }
        }
    }

    func unmanagedSimpleDelete() throws {
        guard let rq = EasyRealmQueue() else { throw EasyRealmError.realmQueueCantBeCreate }
        guard let key = T.primaryKey() else { throw EasyRealmError.objectCantBeResolved }

        try rq.queue.sync {
            let value = self.base.value(forKey: key)
            if let object = rq.realm.object(ofType: T.self, forPrimaryKey: value) {
                try rq.realm.write {
                    EasyRealm.simpleDelete(this: object, in: rq)
                }
            }
        }
    }

    static func simpleDelete(this object: Object, in queue: EasyRealmQueue) {
        queue.realm.delete(object)
    }

}

//Cascade Way
private extension EasyRealm where T: Object {

    func managedCascadeDelete() throws {
        guard let rq = EasyRealmQueue() else { throw EasyRealmError.realmQueueCantBeCreate }
        let ref = ThreadSafeReference(to: self.base)
        try rq.queue.sync {
            guard let object = rq.realm.resolve(ref) else { throw EasyRealmError.objectCantBeResolved }
            try rq.realm.write {
                EasyRealm.cascadeDelete(this: object, in: rq)
            }
        }
    }

    func unmanagedCascadeDelete() throws {
        guard let rq = EasyRealmQueue() else { throw EasyRealmError.realmQueueCantBeCreate }
        guard let key = T.primaryKey() else { throw EasyRealmError.objectCantBeResolved }

        try rq.queue.sync {
            let value = self.base.value(forKey: key)
            if let object = rq.realm.object(ofType: T.self, forPrimaryKey: value) {
                try rq.realm.write {
                    EasyRealm.cascadeDelete(this: object, in: rq)
                }
            }
        }
    }

    static func cascadeDelete(this object: Object, in queue: EasyRealmQueue) {
        for property in object.objectSchema.properties {
            guard !object.isInvalidated, // check invalided state of an object to precent crashes when you would acces one
                  let value = object.value(forKey: property.name) else { continue }
            if let object = value as? Object {
                EasyRealm.cascadeDelete(this: object, in: queue)
            }
            if let list = value as? EasyRealmList {
                list.children().forEach {
                    EasyRealm.cascadeDelete(this: $0, in: queue)
                }
            }
        }

        queue.realm.delete(object)
    }
}
