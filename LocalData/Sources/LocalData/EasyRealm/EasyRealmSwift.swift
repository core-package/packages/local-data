//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import Foundation
import RealmSwift

internal protocol EasyRealmList {
    func children() -> [Object]
}

extension List: EasyRealmList {
    internal func children() -> [Object] {
        return self.compactMap { return $0 as? Object }
    }
}
