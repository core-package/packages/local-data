//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import Foundation
import RealmSwift

public extension EasyRealmStatic where T: Object {

    func fromRealm<K>(with primaryKey: K) throws -> T {
        let realm = try Realm()
        if let object = realm.object(ofType: self.baseType, forPrimaryKey: primaryKey) {
            return object
        } else {
            throw EasyRealmError.objectWithPrimaryKeyNotFound
        }
    }

    func fromRealm(with predicate: NSPredicate) throws -> Results<T> {
        let realm = try Realm()
        return realm.objects(self.baseType).filter(predicate)
    }

    func all() throws -> Results<T> {
        let realm = try Realm()
        return realm.objects(self.baseType)
    }
}
