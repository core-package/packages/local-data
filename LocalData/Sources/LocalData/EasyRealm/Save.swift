//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Foundation
import RealmSwift
  
extension EasyRealm where T: Object {

    public func saved(updatePolicy: Realm.UpdatePolicy) throws -> T {
        return (self.isManaged) ? try managedSave(updatePolicy: updatePolicy) : try unmanagedSave(updatePolicy: updatePolicy)
    }

}

private extension EasyRealm where T: Object {

    func managedSave(updatePolicy: Realm.UpdatePolicy) throws -> T {
        let ref = ThreadSafeReference(to: self.base)
        guard let rq = EasyRealmQueue() else {
            throw EasyRealmError.realmQueueCantBeCreate
        }
        return try rq.queue.sync {
            guard let object = rq.realm.resolve(ref) else { throw EasyRealmError.objectCantBeResolved }
            rq.realm.beginWrite()

            let ret = rq.realm.create(T.self, value: object, update: updatePolicy)
            try rq.realm.commitWrite()
            return ret
        }
    }

    func unmanagedSave(updatePolicy: Realm.UpdatePolicy) throws -> T {
        let realm = try Realm()
        realm.beginWrite()

        let ret = realm.create(T.self, value: self.base, update: updatePolicy)
        try realm.commitWrite()
        return ret
    }

}
