//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import RealmSwift
import Foundation
import Combine

protocol LocalData { }

// MARK: - With Publishers

extension LocalData {

    /// Until now, we have worked with the object extension types, and didn't access realm directly from this aspect of layer
    /// But we need to delete all entries on changing the environment, so this is one of the known options, the other one is to declare object types, iterate through them and call delete all for specific object type
    func deleteAllDatabaseEntries() throws {
        let realm = try Realm()
        
        try realm.write {
            realm.deleteAll()
        }
    }

    // MARK: - Observe

    func observe<Mapable>(type: Mapable.Type) -> (notificationToken: NotificationToken?, publisher: AnyPublisher<[Mapable.RecordType], EasyRealmError>) where Mapable : Object, Mapable : RecordMappable {

        let subject = PassthroughSubject<[Mapable.RecordType], EasyRealmError>()
        let token = observe(type: type, completion: { result  in
            switch result {
            case .success(let objects):
                subject.send(objects)
            case .failure(let error):
                subject.send(completion: .failure(error))
            }
        })
        return (token, subject.eraseToAnyPublisher())
    }

    // MARK: - Get

    func get<Mapable>(type: Mapable.Type) -> AnyPublisher<[Mapable.RecordType], EasyRealmError> where Mapable : Object, Mapable : RecordMappable {
        return Deferred {
            Future<[Mapable.RecordType], EasyRealmError> { promise in
                self.get(type: type, completion: { result in
                    switch result {
                    case .success(let objects):
                        promise(.success(objects))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    func get<Mapable, PrimaryKey>(type: Mapable.Type,
                                   primaryKey: PrimaryKey) -> AnyPublisher<Mapable.RecordType, EasyRealmError> where Mapable : Object, Mapable : RecordMappable {
         return Deferred {
             Future<Mapable.RecordType, EasyRealmError> { promise in
                self.get(type: type, primaryKey: primaryKey, completion: { result in
                     switch result {
                     case .success(let object):
                         promise(.success(object))
                     case .failure(let err):
                         promise(.failure(err))
                     }
                 })
             }
         }
         .eraseToAnyPublisher()
     }

    func get<Mapable>(type: Mapable.Type,
                      predicate: NSPredicate) -> AnyPublisher<[Mapable.RecordType], EasyRealmError> where Mapable : Object, Mapable : RecordMappable {
        return Deferred {
            Future<[Mapable.RecordType], EasyRealmError> { promise in
               self.get(type: type, predicate: predicate, completion: { result in
                    switch result {
                    case .success(let objects):
                        promise(.success(objects))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    func get<Mapable>(type: Mapable.Type,
                      predicate: NSPredicate,
                      sorter: Sorter) -> AnyPublisher<[Mapable.RecordType], EasyRealmError> where Mapable : Object, Mapable : RecordMappable {
        return Deferred {
            Future<[Mapable.RecordType], EasyRealmError> { promise in
                self.get(type: type, predicate: predicate, sorter: sorter, completion: { result in
                    switch result {
                    case .success(let objects):
                        promise(.success(objects))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    // MARK: - Save

    func save<Mapable: RecordMappable>(type: Mapable.Type,
                                      objects: [Mapable.RecordType],
                                      updatePolicy: Realm.UpdatePolicy) -> AnyPublisher<[Mapable.RecordType], EasyRealmError> where Mapable: Object {
        return Deferred {
            Future<[Mapable.RecordType], EasyRealmError> { promise in
                self.save(type: type, objects: objects, updatePolicy: updatePolicy, completion: { result in
                    switch result {
                    case .success(let objects):
                        promise(.success(objects))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    func save<Mapable: RecordMappable>(type: Mapable.Type,
                                      object: Mapable.RecordType,
                                      updatePolicy: Realm.UpdatePolicy) -> AnyPublisher<Mapable.RecordType, EasyRealmError> where Mapable: Object {
        return Deferred {
            Future<Mapable.RecordType, EasyRealmError> { promise in
                self.save(type: type, object: object, updatePolicy: updatePolicy, completion: { result in
                    switch result {
                    case .success(let object):
                        promise(.success(object))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    // MARK: - Delete

    func delete<Mapable: RecordMappable>(type: Mapable.Type,
                                         object: Mapable.RecordType) -> AnyPublisher<(), EasyRealmError> where Mapable: Object {
        return Deferred {
            Future<(), EasyRealmError> { promise in
                self.delete(type: type, object: object, completion: { result in
                    switch result {
                    case .success:
                        promise(.success(()))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                })
            }
        }
        .eraseToAnyPublisher()
    }

    func deleteAllEntries() -> AnyPublisher<(), EasyRealmError> {
        return Deferred {
            Future<(), EasyRealmError> { promise in
                self.deleteAllEntries { result in
                    switch result {
                    case .success:
                        promise(.success(()))
                    case .failure(let err):
                        promise(.failure(err))
                    }
                }
            }
        }
        .eraseToAnyPublisher()
    }
}

// MARK: - With completions

extension LocalData {

    // MARK: - Observe

    func observe<Mapable>(type: Mapable.Type,
                          completion: @escaping (Result<[Mapable.RecordType], EasyRealmError>) -> Void) -> NotificationToken? where Mapable : Object, Mapable : RecordMappable {
        return try? type.er.all().observe({ observedResult in
            switch observedResult {
            case .initial(let objs),
                 .update(let objs, _, _, _):
                let objs: [Mapable.RecordType] =  objs.map { $0.toRecord() }.compactMap { $0 }
                completion(.success(objs))
            case .error:
                completion(.failure(.objectCantBeResolved))
            }
        })
    }

    // MARK: - Get

    func get<Mapable>(type: Mapable.Type,
                      completion: @escaping (Result<[Mapable.RecordType], EasyRealmError>) -> Void) where Mapable : Object, Mapable : RecordMappable {
        if let result: [Mapable.RecordType] = try? type.er.all().map({ $0.er.unmanaged.toRecord() }).compactMap({ $0 }) {
            completion(.success(result))
        } else {
            completion(.failure(.objectCantBeResolved))
        }
    }

    func get<Mapable, PrimaryKey>(type: Mapable.Type,
                                  primaryKey: PrimaryKey,
                                  completion: @escaping (Result<Mapable.RecordType, EasyRealmError>) -> Void) where Mapable : Object, Mapable : RecordMappable {
        if let obj = try? type.er.fromRealm(with: primaryKey).toRecord() {
            completion(.success(obj))
        } else {
            completion(.failure(.objectWithPrimaryKeyNotFound))
        }
    }

    func get<Mapable>(type: Mapable.Type,
                      predicate: NSPredicate,
                      completion: @escaping (Result<[Mapable.RecordType], EasyRealmError>) -> Void) where Mapable : Object, Mapable : RecordMappable {
        if let objects: [Mapable.RecordType] = try? type.er.fromRealm(with: predicate).map({ $0.toRecord() }).compactMap({ $0 }) {
            completion(.success(objects))
        } else {
            completion(.failure(.objectCantBeResolved))
        }
    }

    func get<Mapable>(type: Mapable.Type,
                      predicate: NSPredicate,
                      sorter: Sorter,
                      completion: @escaping (Result<[Mapable.RecordType], EasyRealmError>) -> Void) where Mapable : Object, Mapable : RecordMappable {
        if let objects: [Mapable.RecordType] = try? type.er.fromRealm(with: predicate).sorted(byKeyPath: sorter.keyPath, ascending: sorter.order == .ascending).map({ $0.toRecord() }).compactMap({ $0 }) {
            completion(.success(objects))
        } else {
            completion(.failure(.objectCantBeResolved))
        }
    }

    // MARK: - Save

    func save<Mapable: RecordMappable>(type: Mapable.Type,
                                      objects: [Mapable.RecordType],
                                      updatePolicy: Realm.UpdatePolicy,
                                      completion: (((Result<[Mapable.RecordType], EasyRealmError>)) -> Void)?) where Mapable: Object {
        if let objs: [Mapable.RecordType] = try? objects.map({ try type.fromRecord($0).er.saved(updatePolicy: updatePolicy).er.unmanaged.toRecord() }).compactMap({ $0 }) {
            completion?(.success(objs))
        } else {
            completion?(.failure(.objectCantBeResolved))
        }
    }

    func save<Mapable: RecordMappable>(type: Mapable.Type,
                                      object: Mapable.RecordType,
                                      updatePolicy: Realm.UpdatePolicy,
                                      completion: (((Result<Mapable.RecordType, EasyRealmError>)) -> Void)?) where Mapable: Object {
        if let obj: Mapable.RecordType = try? type.fromRecord(object)
            .er.saved(updatePolicy: updatePolicy)
            .er.unmanaged.toRecord() {
            completion?(.success(obj))
        } else {
            completion?(.failure(.objectCantBeResolved))
        }
    }

    func delete<Mapable: RecordMappable>(type: Mapable.Type,
                                         object: Mapable.RecordType,
                                         completion: (((Result<(), EasyRealmError>)) -> Void)?) where Mapable: Object {
        do {
            try type.fromRecord(object).er.delete(with: .cascade)
            completion?(Result.success(()))
        } catch {
            completion?(.failure(.objectCantBeResolved))
        }
    }

    func deleteAllEntries(completion: (((Result<(), EasyRealmError>)) -> Void)?) {
        do {
            try deleteAllDatabaseEntries()
            completion?(Result.success(()))
        } catch {
            completion?(.failure(.objectCantBeResolved))
        }
    }
}


extension LocalData {
    
    func deleteAll<Mapable: Object & RecordMappable>(type: Mapable.Type) -> AnyPublisher<Void, EasyRealmError> {
        return Deferred {
            Future<Void, EasyRealmError> { promise in
                do {
                    let realm = try Realm()
                    let objects = realm.objects(type)
                    try realm.write {
                        realm.delete(objects)
                    }
                    promise(.success(()))
                } catch {
                    promise(.failure(.objectCantBeResolved))
                }
            }
        }
        .eraseToAnyPublisher()
    }
}
