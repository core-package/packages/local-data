# CoreConcept LocalData Package

#### Table of Contents
Description
Advantages
Defining Models
Usage within a ViewModel

## Description

 The CoreConcept LocalData Package is a fully tested and ready-to-use Swift package designed to simplify local data management in your iOS projects. With this package, you can easily interact with your local Realm database, allowing you to perform common operations such as saving, retrieving, observing, and deleting objects in a clean and efficient manner.
 
 This package is designed with a focus on testability and ease of use, making it an ideal choice for developers who want a robust and reliable solution for managing local data in their applications.

## Advantages
By using the CoreConcept LocalData Package in your project, you can:
- Save time by using a pre-built, fully tested solution for local data management. Simplify your codebase and improve readability with a clean and organized architecture.
- Improve maintainability and testability with a modular, reusable package.
- Easily adapt the package to your specific project requirements, thanks to its flexibility and extensibility.

## Defining Models
###### To use the CoreConcept LocalData Package, you need to define your data models according to the following requirements:

Your models should inherit from **Object**. Implement the RecordMappable protocol. Define a nested RecordType struct that conforms to Codable. Here's an example of how to define a model:

```swift
import Foundation
import RealmSwift

final class YourModel: Object, RecordMappable {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }

    struct RecordType: Codable {
        var id: String
        var name: String
    }

    func toRecord() -> RecordType? {
        return RecordType(id: id, name: name)
    }

    static func fromRecord(_ record: RecordType) -> Self {
        let model = YourModel()
        model.id = record.id
        model.name = record.name
        return model as! Self
    }
}
```
##### Usage within a ViewModel

To use the CoreConcept LocalData Package within a ViewModel, follow these steps:

##### 1. Import the package in your ViewModel file:

```swift
import LocalData
```
##### 2. Create a property for the LocalData instance:
```swift
private let localData: LocalData
```
##### 3. Initialize the LocalData instance in your ViewModel's initializer:
```swift
init(localData: LocalData = LocalDataImpl()) {
    self.localData = localData
}
```
##### Interact with the local data using the provided methods. For example, to save an object:
```swift
localData.save(type: YourModel.self, object: yourModelObject, updatePolicy: .all)
    .sink { completion in
        // Handle completion
    } receiveValue: { _ in
        // Handle value
    }
    .store(in: &cancellables)
```
##### 5. To observe changes in your local data:  
```swift
var token: NotificationToken?
let publisher = localData.observe(type: YourModel.self).publisher

publisher.sink { completion in
    // Handle completion
} receiveValue: { objects in
    // Handle value
    .store(in: &cancellables)

token = localData.observe(type: YourModel.self).notificationToken
}
```
##### To delete an object:
```swift
localData.delete(type: YourModel.self, predicate: NSPredicate(format: "id == %@", objectId))
    .sink { completion in
        // Handle completion
    } receiveValue: { _ in
        // Handle value
    }
    .store(in: &cancellables)
```
##### To retrieve objects: 
```swift
localData.retrieve(type: YourModel.self, predicate: NSPredicate(format: "name CONTAINS[cd] %@", searchText))
    .sink { completion in
        // Handle completion
    } receiveValue: { objects in
        // Handle value
    }
    .store(in: &cancellables)
```
