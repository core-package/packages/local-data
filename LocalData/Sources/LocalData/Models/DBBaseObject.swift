//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import RealmSwift
import Foundation

class DBBaseObject: Object {

    @objc dynamic open var primaryKey = UUID().uuidString

    public override static func primaryKey() -> String? {
        return "primaryKey"
    }
}
