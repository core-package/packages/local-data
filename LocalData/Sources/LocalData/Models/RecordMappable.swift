//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import Foundation

protocol RecordMappable {
    associatedtype RecordType

    static func fromRecord(_ record: RecordType) -> Self
    func toRecord() -> RecordType?
}
