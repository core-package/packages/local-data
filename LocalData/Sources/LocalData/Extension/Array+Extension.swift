//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import RealmSwift

public extension Array where Element: Object {

    /// Warning: watch out with using this one. It works well for initializing relationships, but when you need to modify a relation, use .append(objects:) instead since using this will not work
    var asRealmList: List<Element> {
        let result = List<Element>()
        result.append(objectsIn: self)
        return result
    }
}
