import XCTest
import Combine
@testable import LocalData

class LocalDataTests: XCTestCase {
    var localData: LocalData!
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        localData = LocalDataImpl()
        cancellables = []
    }
    
    override func tearDown() {
        localData = nil
        cancellables = nil
        super.tearDown()
    }
    
    func testDeleteAllDatabaseEntries() {
        XCTAssertNoThrow(try localData.deleteAllDatabaseEntries())
    }
    
    func testSave() {
        let expectation = XCTestExpectation(description: "Save object")
        
        let testRecord = TestModel.RecordType(id: UUID().uuidString, name: "Test Name")
        localData.save(type: TestModel.self, object: testRecord, updatePolicy: .all)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Save failed with error: \(error)")
                }
                expectation.fulfill()
            } receiveValue: { _ in }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 5.0)
        testDeleteAll()
    }
    
    func testSaveTwoAndObserve() {
        let saveExpectation = XCTestExpectation(description: "Save two objects")
        let testRecord1 = TestModel.RecordType(id: UUID().uuidString, name: "Test Name 1")
        let testRecord2 = TestModel.RecordType(id: UUID().uuidString, name: "Test Name 2")

        Publishers.Zip(
            localData.save(type: TestModel.self, object: testRecord1, updatePolicy: .all),
            localData.save(type: TestModel.self, object: testRecord2, updatePolicy: .all)
        )
        .sink { completion in
            switch completion {
            case .finished:
                break
            case .failure(let error):
                XCTFail("Save failed with error: \(error)")
            }
            saveExpectation.fulfill()
        } receiveValue: { _, _ in }
        .store(in: &cancellables)

        wait(for: [saveExpectation], timeout: 5.0)

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let observeExpectation = XCTestExpectation(description: "Observe saved objects")
            var token: NotificationToken?
            let publisher = self.localData.observe(type: TestModel.self).publisher

            publisher.sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Observe failed with error: \(error)")
                }
                token?.invalidate()
                observeExpectation.fulfill()
            } receiveValue: { objects in
                XCTAssertEqual(objects.count, 2)
            }
            .store(in: &self.cancellables)

            token = self.localData.observe(type: TestModel.self).notificationToken
            self.wait(for: [observeExpectation], timeout: 5.0)
        }

    }
    
    func testGet() {
        let expectation = XCTestExpectation(description: "Get objects")
        
        localData.get(type: TestModel.self)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Get failed with error: \(error)")
                }
                expectation.fulfill()
            } receiveValue: { objects in
                XCTAssertTrue(objects.isEmpty)
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    // ... Add similar tests for other methods like `get(type:predicate:)`, `save(type:objects:updatePolicy:)`, `delete(type:object:)`, and so on.
    
    func testDeleteAll() {
        let expectation = XCTestExpectation(description: "Delete all objects")
        
        localData.deleteAll(type: TestModel.self)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Delete all failed with error: \(error)")
                }
                expectation.fulfill()
            } receiveValue: {
                XCTAssert(true)
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
    }
}

import Foundation
import RealmSwift

final class TestModel: Object, RecordMappable {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }

    struct RecordType: Codable {
        var id: String
        var name: String
    }

    func toRecord() -> RecordType? {
        return RecordType(id: id, name: name)
    }

    static func fromRecord(_ record: RecordType) -> Self {
        let model = TestModel()
        model.id = record.id
        model.name = record.name
        return model as! Self
    }
}
