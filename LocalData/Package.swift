// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LocalData",
    platforms: [
        .iOS(.v13), // Adjust this to the minimum iOS version you want to support
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LocalData",
            targets: ["LocalData"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
//        .package(url: "https://github.com/realm/realm-cocoa.git", from: "10.0.0"),
        .package(url: "https://github.com/istvan-kreisz/CombineRealm.git", from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "LocalData",
            dependencies: [
//                .product(name: "RealmSwift", package: "realm-cocoa"),
                .product(name: "CombineRealm", package: "CombineRealm")
            ]
        ),
        .testTarget(
            name: "LocalDataTests",
            dependencies: ["LocalData"]),
    ])
